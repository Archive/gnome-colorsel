/* gtkcolortriangle.h
 * Copyright (C) 1999 Red Hat, Inc. and Simon Budig
 * GtkColorTriangle developed by Havoc Pennington
 * Original code Copyright 1998, Simon Budig <Simon.Budig@unix-ag.org>,
 * used under the LGPL by permission.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTK_COLOR_TRIANGLE_H__
#define __GTK_COLOR_TRIANGLE_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define GTK_TYPE_COLOR_TRIANGLE			(gtk_color_triangle_get_type ())
#define GTK_COLOR_TRIANGLE(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_COLOR_TRIANGLE, GtkColorTriangle))
#define GTK_COLOR_TRIANGLE_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_COLOR_TRIANGLE, GtkColorTriangleClass))
#define GTK_IS_COLOR_TRIANGLE(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_COLOR_TRIANGLE))
#define GTK_IS_COLOR_TRIANGLE_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_COLOR_TRIANGLE))


typedef struct _GtkColorTriangle       GtkColorTriangle;
typedef struct _GtkColorTriangleClass  GtkColorTriangleClass;

struct _GtkColorTriangle
{
  GtkDrawingArea drawing_area;

  /*< private >*/

  guchar* buf;

  gint rgb_width;
  gint rgb_rowstride;
  gint rgb_height;
  
  gint wheel_radius;
  gint triangle_radius;
  gint wheel_width;
  
  GtkUpdateType policy;
  gint timer_tag;

  gint last_x;
  gint last_y;

  /* Hue, Saturation, Value, Red, Green, Blue */
  /* 0-360, 0-100, 0-100, 0-255, 0-255, 0-255 */
  gint values[6];

  gdouble oldsat;
  gdouble oldval;

  GdkCursor* crosshair;
  
  guint hue_has_changed : 1;
  guint dragging_in_wheel : 1;
  guint dragging_in_triangle : 1;
  guint update_pending : 1;
};
struct _GtkColorTriangleClass
{
  GtkDrawingAreaClass parent_class;

  void (*color_changed)	(GtkColorTriangle *color_triangle);
};


GtkType    gtk_color_triangle_get_type (void);
GtkWidget *gtk_color_triangle_new      (void);

void       gtk_color_triangle_set_update_policy (GtkColorTriangle     *colorsel,
                                                 GtkUpdateType         policy);
void       gtk_color_triangle_set_color         (GtkColorTriangle     *colorsel,
                                                 gdouble              *color);
void       gtk_color_triangle_get_color         (GtkColorTriangle     *colorsel,
                                                 gdouble              *color);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_COLOR_TRIANGLE_H__ */
