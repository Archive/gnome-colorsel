
#include <stdio.h>
#include <gtk/gtk.h>
#include "gtkcolortriangle.h"

gint de_cb(GtkWidget* window, GdkEventAny* event, gpointer data)
{
  exit(0);
  return TRUE;
}

static void
color_change_cb(GtkWidget* triangle, gpointer data)
{
  gdouble color[3];

  gtk_color_triangle_get_color(GTK_COLOR_TRIANGLE(triangle),
                               color);

  printf("%g %g %g\n",
         color[0], color[1], color[2]);
}

int main (int argc, char *argv[])
{
  GtkWidget* win;
  GtkWidget* tri;
  
  gtk_set_locale ();
  
  gtk_init (&argc, &argv);

  gdk_rgb_init();
  
  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect(GTK_OBJECT(win), "delete_event", GTK_SIGNAL_FUNC(de_cb), NULL);
  
  tri = gtk_color_triangle_new();

  gtk_drawing_area_size(GTK_DRAWING_AREA(tri), 200, 200);
  
  gtk_container_add(GTK_CONTAINER(win), tri);

  gtk_signal_connect(GTK_OBJECT(tri), "color_changed",
                     GTK_SIGNAL_FUNC(color_change_cb),
                     NULL);

#if 0
  gtk_color_triangle_set_update_policy(GTK_COLOR_TRIANGLE(tri),
                                       GTK_UPDATE_DELAYED);
#endif
  
  gtk_widget_show_all(win);

  gtk_main ();

  return 0;
}
