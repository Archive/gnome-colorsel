/* gtk-color-selection2.h
 * Copyright (C) 1999  J. Arthur Random
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_COLOR_SELECTION2_H__
#define __GTK_COLOR_SELECTION2_H__

#include <gtk/gtkvbox.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_TYPE_COLOR_SELECTION2			(gtk_color_selection2_get_type ())
#define GTK_COLOR_SELECTION2(obj)			(GTK_CHECK_CAST ((obj), GTK_TYPE_COLOR_SELECTION2, GtkColorSelection2))
#define GTK_COLOR_SELECTION2_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_COLOR_SELECTION2, GtkColorSelection2Class))
#define GTK_IS_COLOR_SELECTION2(obj)			(GTK_CHECK_TYPE ((obj), GTK_TYPE_COLOR_SELECTION2))
#define GTK_IS_COLOR_SELECTION2_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GTK_TYPE_COLOR_SELECTION2))

/* Number of elements in the custom palatte */
#define CUSTOM_PALETTE_WIDTH 10
#define CUSTOM_PALETTE_HEIGHT 2

typedef struct _GtkColorSelection2       GtkColorSelection2;
typedef struct _GtkColorSelection2Class  GtkColorSelection2Class;

struct _GtkColorSelection2
{
  GtkVBox parent;

  /* < private > */
  gpointer private;
};
struct _GtkColorSelection2Class
{
  GtkVBoxClass parent_class;

  void (*color_changed)	(GtkColorSelection2 *color_selection2);
};

GtkType    gtk_color_selection2_get_type (void);
GtkWidget *gtk_color_selection2_new      (void);
void       gtk_color_selection2_set_update_policy  (GtkColorSelection2    *colorsel,
						    GtkUpdateType          policy);
/* move to gtk-compat */
#define    gtk_color_selection2_set_opacity(colorsel, use_opacity) gtk_color_selection2_set_use_opacity(colorsel, use_opacity)
gboolean   gtk_color_selection2_get_use_opacity    (GtkColorSelection2    *colorsel);
void       gtk_color_selection2_set_use_opacity    (GtkColorSelection2    *colorsel,
						    gboolean               use_opacity);
gboolean   gtk_color_selection2_get_use_palette    (GtkColorSelection2    *colorsel);
void       gtk_color_selection2_set_use_palette    (GtkColorSelection2    *colorsel,
						    gboolean               use_palette);

/* The Color set is an array of doubles, of the following format:
 * color[0] = red_channel;
 * color[1] = green_channel;
 * color[2] = blue_channel;
 * color[3] = alpha_channel;
 */
void       gtk_color_selection2_set_color          (GtkColorSelection2    *colorsel,
						    gdouble               *color);
void       gtk_color_selection2_get_color          (GtkColorSelection2    *colorsel,
						    gdouble               *color);
void       gtk_color_selection2_set_old_color      (GtkColorSelection2    *colorsel,
						    gdouble               *color);
void       gtk_color_selection2_get_old_color      (GtkColorSelection2    *colorsel,
						    gdouble               *color);
void       gtk_color_selection2_set_palette_color  (GtkColorSelection2   *colorsel,
					 	    gint                  x,
						    gint                  y,
						    gdouble              *color);
gboolean   gtk_color_selection2_get_palette_color  (GtkColorSelection2   *colorsel,
						    gint                  x,
						    gint                  y,
						    gdouble              *color);
void       gtk_color_selection2_unset_palette_color(GtkColorSelection2   *colorsel,
						    gint                  x,
						    gint                  y);
gboolean   gtk_color_selection2_is_adjusting       (GtkColorSelection2    *colorsel);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_COLOR_SELECTION2_H__ */
