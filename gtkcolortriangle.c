/* gtkcolortriangle.c
 * Copyright (C) 1999 Red Hat, Inc. and Simon Budig
 * GtkColorTriangle developed by Havoc Pennington
 * Original code Copyright 1998, Simon Budig <Simon.Budig@unix-ag.org>,
 * used under the LGPL by permission.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "gtkcolortriangle.h"
#include <math.h>
#include <stdio.h>
#include <gdk/gdkrgb.h>

#ifndef M_PI
#define M_PI    3.14159265358979323846
#endif /* M_PI */

#define TIMER_DELAY 300

enum {
  HUE = 0,
  SATURATION,
  VALUE,
  RED,
  GREEN,
  BLUE,
  HUE_SATURATION,
  HUE_VALUE,
  SATURATION_VALUE,
  RED_GREEN,
  RED_BLUE,
  GREEN_BLUE
};

enum {
  COLOR_CHANGED,
  LAST_SIGNAL
};

#define INTENSITY(r,g,b) (r * 0.30 + g * 0.59 + b * 0.11 + 0.001)

#define BGCOLOR 180

#define MARKER_SIZE 20

static void gtk_color_triangle_init		(GtkColorTriangle		 *color_triangle);
static void gtk_color_triangle_class_init	(GtkColorTriangleClass	 *klass);

static gint gtk_color_triangle_button_press(GtkWidget* widget, GdkEventButton* event);
static gint gtk_color_triangle_button_release(GtkWidget* widget, GdkEventButton* event);
static gint gtk_color_triangle_motion_notify(GtkWidget* widget, GdkEventMotion* event);

static void gtk_color_triangle_realize(GtkWidget* widget);
static void gtk_color_triangle_unrealize(GtkWidget* widget);
static void gtk_color_triangle_size_allocate(GtkWidget* widget, GtkAllocation* allocation);
static gint gtk_color_triangle_expose (GtkWidget      *widget, GdkEventExpose *event);
static void gtk_color_triangle_color_changed (GtkColorTriangle *color_triangle);

/* Update RGB from HSV */
static void gtk_color_triangle_update_rgb_values (GtkColorTriangle* color_triangle);
/* vice-versa */
static void gtk_color_triangle_update_hsv_values (GtkColorTriangle* color_triangle);
static void gtk_color_triangle_finalize(GtkObject* object);

static GtkDrawingAreaClass *parent_class = NULL;
static guint color_triangle_signals[LAST_SIGNAL] = { 0 };

GtkType
gtk_color_triangle_get_type (void)
{
  static GtkType color_triangle_type = 0;

  if (!color_triangle_type)
    {
      static const GtkTypeInfo color_triangle_info =
      {
        "GtkColorTriangle",
        sizeof (GtkColorTriangle),
        sizeof (GtkColorTriangleClass),
        (GtkClassInitFunc) gtk_color_triangle_class_init,
        (GtkObjectInitFunc) gtk_color_triangle_init,
        /* reserved_1 */ NULL,
        /* reserved_2 */ NULL,
        (GtkClassInitFunc) NULL,
      };

      color_triangle_type = gtk_type_unique (gtk_drawing_area_get_type (), &color_triangle_info);
    }

  return color_triangle_type;
}

static void
gtk_color_triangle_class_init (GtkColorTriangleClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  
  parent_class = gtk_type_class (gtk_drawing_area_get_type ());

  color_triangle_signals[COLOR_CHANGED] = 
    gtk_signal_new ("color_changed",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (GtkColorTriangleClass, color_changed),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);
  

  gtk_object_class_add_signals (object_class, color_triangle_signals, LAST_SIGNAL);

  object_class->finalize = gtk_color_triangle_finalize;

  widget_class->realize = gtk_color_triangle_realize;
  widget_class->unrealize = gtk_color_triangle_unrealize;
  widget_class->button_press_event = gtk_color_triangle_button_press;
  widget_class->button_release_event = gtk_color_triangle_button_release;
  widget_class->motion_notify_event = gtk_color_triangle_motion_notify;	
  widget_class->expose_event = gtk_color_triangle_expose;
  
  widget_class->size_allocate = gtk_color_triangle_size_allocate;
}


static void
gtk_color_triangle_init (GtkColorTriangle *color_triangle)
{
  gtk_widget_add_events(GTK_WIDGET(color_triangle),
                        GDK_EXPOSURE_MASK | 
                        GDK_BUTTON_PRESS_MASK | 
                        GDK_BUTTON_RELEASE_MASK | 
                        GDK_BUTTON_MOTION_MASK);
  color_triangle->policy = GTK_UPDATE_CONTINUOUS;

  color_triangle->values[HUE] = 0;
  color_triangle->values[SATURATION] = 100;
  color_triangle->values[VALUE] = 100;
  gtk_color_triangle_update_rgb_values(color_triangle);
  
  color_triangle->oldsat = 0.0;
  color_triangle->oldval = 0.0;
  
  color_triangle->wheel_width = 20;
  
  color_triangle->buf = NULL;
}

static void
gtk_color_triangle_finalize(GtkObject* object)
{
  GtkColorTriangle* color_triangle;
  
  color_triangle = GTK_COLOR_TRIANGLE(object);
  
  if (color_triangle->buf)
    g_free(color_triangle->buf);

  if (GTK_OBJECT_CLASS(parent_class)->finalize)
    (*GTK_OBJECT_CLASS(parent_class)->finalize)(object);
}

static void
gtk_color_triangle_color_changed (GtkColorTriangle *color_triangle)
{
  gtk_signal_emit (GTK_OBJECT (color_triangle), color_triangle_signals[COLOR_CHANGED]);
}

static void
gtk_color_triangle_realize(GtkWidget* widget)
{
  GtkColorTriangle* color_triangle;

  (*GTK_WIDGET_CLASS(parent_class)->realize)(widget);

  color_triangle = GTK_COLOR_TRIANGLE(widget);

  color_triangle->crosshair = gdk_cursor_new(GDK_CROSSHAIR);
}

static void
gtk_color_triangle_unrealize(GtkWidget* widget)
{
  GtkColorTriangle* color_triangle;

  (*GTK_WIDGET_CLASS(parent_class)->unrealize)(widget);

  color_triangle = GTK_COLOR_TRIANGLE(widget);

  gdk_cursor_destroy(color_triangle->crosshair);
  color_triangle->crosshair = NULL;
}

static void
gtk_color_triangle_size_allocate(GtkWidget* widget, GtkAllocation* allocation)
{
  GtkColorTriangle* color_triangle;
  guchar* buf;
  gint square_side;
  guint bufsize;
  
  color_triangle = GTK_COLOR_TRIANGLE(widget);

  (*GTK_WIDGET_CLASS(parent_class)->size_allocate)(widget, allocation);

  square_side = MIN(widget->allocation.width, widget->allocation.height);
  
  color_triangle->rgb_width = widget->allocation.width;

  color_triangle->wheel_radius = square_side / 2;

  /* Try for a 20-pixel wheel, otherwise bail and split the available
   *  radius in half between wheel and triangle. Always use an odd number.
   */
  color_triangle->wheel_width = 20;
  
  if (color_triangle->wheel_radius > color_triangle->wheel_width)
    color_triangle->triangle_radius = color_triangle->wheel_radius - color_triangle->wheel_width;
  else
    {
      color_triangle->triangle_radius = square_side / 4;
      color_triangle->wheel_width = square_side / 4;
    }
  
  /* FIXME pick an efficient rowstride */
  color_triangle->rgb_rowstride = color_triangle->rgb_width * 3;

  color_triangle->rgb_height = widget->allocation.height;
  
  printf(" trad: %d wrad: %d rwid: %u rowstride: %u\n", color_triangle->triangle_radius,
         color_triangle->wheel_radius, color_triangle->rgb_width, color_triangle->rgb_rowstride);

  bufsize = (color_triangle->rgb_rowstride * color_triangle->rgb_height);
  
  buf = g_malloc(bufsize);

  memset(buf, BGCOLOR, bufsize);

  if (color_triangle->buf != NULL)
    g_free(color_triangle->buf);
  
  color_triangle->buf = buf;
  
  color_triangle->update_pending = TRUE;
  color_triangle->hue_has_changed = TRUE;
  
  gtk_widget_queue_draw (widget);
}

static void
color_hsv_to_rgb (gdouble hue, gdouble sat, gdouble val, guchar* red, guchar *green, guchar *blue)
{
  gdouble f, p, q, t;

  if (sat == 0) {
    *red = val * 255;
    *green = val * 255;
    *blue = val * 255;
  } else {
    while (hue < 0)
      hue += 360;
    while (hue >= 360)
      hue -= 360;
      
    hue /= 60;
    f = hue - (int) hue;
    p = val * (1 - sat);
    q = val * (1 - (sat * f));
    t = val * (1 - (sat * (1 - f)));

    switch ((int) hue) {
    case 0:
      *red = val * 255;
      *green = t * 255;
      *blue = p * 255;
      break;
    case 1:
      *red = q * 255;
      *green = val * 255;
      *blue = p * 255;
      break;
    case 2:
      *red = p * 255;
      *green = val * 255;
      *blue = t * 255;
      break;
    case 3:
      *red = p * 255;
      *green = q * 255;
      *blue = val * 255;
      break;
    case 4:
      *red = t * 255;
      *green = p * 255;
      *blue = val * 255;
      break;
    case 5:
      *red = val * 255;
      *green = p * 255;
      *blue = q * 255;
      break;
    default:
      break;
    }
  }
}

static void
color_rgb_to_hsv (guchar r, guchar g, guchar b,
                  gdouble *h, gdouble *s, gdouble *v)
{
  gdouble max, min, delta;

  max = r;
  if (g > max)
    max = g;
  if (b > max)
    max = b;

  min = r;
  if (g < min)
    min = g;
  if (b < min)
    min = b;

  *v = max;

  if (max != 0.0)
    *s = (max - min) / max;
  else
    *s = 0.0;

  if (*s == 0.0)
    *h = -1.0;
  else
    {
      delta = max - min;

      if (r == max)
	*h = (g - b) / delta;
      else if (g == max)
	*h = 2.0 + (b - r) / delta;
      else if (b == max)
	*h = 4.0 + (r - g) / delta;

      *h = *h * 60.0;

      if (*h < 0.0)
	*h = *h + 360;
    }
}


/* Update RGB from HSV */
static void
gtk_color_triangle_update_rgb_values (GtkColorTriangle* color_triangle)
{
  gdouble h, s, v;
  guchar r, g, b;

  h = color_triangle->values[HUE];
  s = color_triangle->values[SATURATION] / 100.0;
  v = color_triangle->values[VALUE] / 100.0;

  color_hsv_to_rgb(h, s, v, &r, &g, &b);
  
  color_triangle->values[RED] = r;
  color_triangle->values[GREEN] = g;
  color_triangle->values[BLUE] = b;
}

static void
gtk_color_triangle_update_hsv_values (GtkColorTriangle* color_triangle)
{
  gdouble h, s, v;

  
  color_rgb_to_hsv(color_triangle->values[RED],
                   color_triangle->values[GREEN],
                   color_triangle->values[BLUE],
                   &h, &s, &v);

  color_triangle->values[HUE] = (gint) h;
  color_triangle->values[SATURATION] = (gint) s;
  color_triangle->values[VALUE] = (gint)v;
}

/* Update color_triangle->values from current marker positions */
static void
gtk_color_triangle_update_values(GtkColorTriangle* color_triangle)
{
  gdouble hue, sat, val;
  gint hx,hy, sx,sy, vx,vy;
  const gint x = color_triangle->last_x;
  const gint y = color_triangle->last_y;

  color_triangle->update_pending = TRUE;
  
  if (color_triangle->dragging_in_wheel)
    {
      color_triangle->values[HUE] = ( (int) (atan2 (x, y) / M_PI * 180) + 360 ) % 360;
      gtk_color_triangle_update_rgb_values(color_triangle);
      color_triangle->hue_has_changed = TRUE;
    }
  else if (color_triangle->dragging_in_triangle)
    {
      hue = (gdouble) color_triangle->values[HUE] * M_PI / 180;
      hx = sin(hue) * color_triangle->triangle_radius;
      hy = cos(hue) * color_triangle->triangle_radius;
      sx = sin(hue - 2*M_PI/3) * color_triangle->triangle_radius;
      sy = cos(hue - 2*M_PI/3) * color_triangle->triangle_radius;
      vx = sin(hue + 2*M_PI/3) * color_triangle->triangle_radius;
      vy = cos(hue + 2*M_PI/3) * color_triangle->triangle_radius;
      hue = (gdouble) color_triangle->values[HUE];

      if ((x-sx)*vx+(y-sy)*vy < 0)
        {
          sat = 1;
          val = ((gdouble) ((x-sx)*(hx-sx)+(y-sy)*(hy-sy)))/((hx-sx)*(hx-sx)+(hy-sy)*(hy-sy));
          if (val<0)
            val=0;
          else if (val>1)
            val=1;
        }
      else if ((x-sx)*hx+(y-sy)*hy < 0)
        {
          sat = 0;
          val = ((gdouble) ((x-sx)*(vx-sx)+(y-sy)*(vy-sy)))/((vx-sx)*(vx-sx)+(vy-sy)*(vy-sy));
          if (val<0)
            val=0;
          else if (val>1)
            val=1;
      }
      else if ((x-hx)*sx+(y-hy)*sy < 0)
        {
          val = 1;
          sat = ((gdouble) ((x-vx)*(hx-vx)+(y-vy)*(hy-vy)))/((hx-vx)*(hx-vx)+(hy-vy)*(hy-vy));
          if (sat<0)
            sat=0;
          else if (sat>1)
            sat=1;
        }
      else
        {
          val = (gdouble) ((x-sx)*(hy-vy)-(y-sy)*(hx-vx)) / (gdouble) ((vx-sx)*(hy-vy)-(vy-sy)*(hx-vx));
          if (val<=0)
            {
              val=0;
              sat=0;
            }
          else
            {
              if (val>1)
                val=1;

              sat = (gdouble) (y-sy-val*(vy-sy)) / (val * (gdouble) (hy-vy));

              if (sat<0)
                sat=0;
              else if (sat>1)
                sat=1;
            }
        }

      color_triangle->values[SATURATION]=100*sat+0.5;
      color_triangle->values[VALUE]=100*val+0.5;
      gtk_color_triangle_update_rgb_values(color_triangle);
    }
}

static gint
gtk_color_triangle_value_timeout (GtkColorTriangle *color_triangle)
{
  gtk_color_triangle_color_changed (color_triangle);

  return TRUE;
}


static gint
gtk_color_triangle_button_press(GtkWidget* widget, GdkEventButton* event)
{
  gint x, y;
  gdouble r;
  GtkColorTriangle* color_triangle;
  
  color_triangle = GTK_COLOR_TRIANGLE(widget);
  
  color_triangle->last_x = x = event->x - color_triangle->wheel_radius - 1;
  color_triangle->last_y = y = - event->y + color_triangle->wheel_radius + 1;
  r = sqrt((gdouble) (x*x+y*y));
  if ( /* r <= color_triangle->wheel_radius  && */ r > color_triangle->triangle_radius)
    {
      color_triangle->dragging_in_wheel = TRUE;
      color_triangle->dragging_in_triangle = FALSE;
    }
  else
    {
      color_triangle->dragging_in_wheel = FALSE;
      color_triangle->dragging_in_triangle = TRUE;
    }

  gdk_window_set_cursor(widget->window, color_triangle->crosshair);
  
  gtk_color_triangle_update_values(color_triangle);

  gtk_color_triangle_color_changed(color_triangle);
  
  gtk_widget_queue_draw(widget);

  return TRUE;
}

static gint
gtk_color_triangle_button_release(GtkWidget* widget, GdkEventButton* event)
{
  GtkColorTriangle* color_triangle;
  
  color_triangle = GTK_COLOR_TRIANGLE(widget);

  color_triangle->dragging_in_wheel = FALSE;
  color_triangle->dragging_in_triangle = FALSE;

  if (color_triangle->timer_tag != 0)
    gtk_timeout_remove (color_triangle->timer_tag);
  color_triangle->timer_tag = 0;

  gdk_window_set_cursor(widget->window, NULL);
  
  gtk_color_triangle_color_changed(color_triangle);
  
  return TRUE;
}

static gint
gtk_color_triangle_motion_notify(GtkWidget* widget, GdkEventMotion* event)
{  
  GtkColorTriangle* color_triangle;
  gint x, y;
  
  color_triangle = GTK_COLOR_TRIANGLE(widget);

  gtk_widget_get_pointer(widget, &x, &y);
  
  color_triangle->last_x = x - color_triangle->wheel_radius - 1;
  color_triangle->last_y = - y + color_triangle->wheel_radius + 1; 

  gtk_color_triangle_update_values(color_triangle);

  switch (color_triangle->policy)
    {
    case GTK_UPDATE_CONTINUOUS:
      gtk_color_triangle_color_changed(color_triangle);
      break;
    case GTK_UPDATE_DELAYED:
      if (color_triangle->timer_tag != 0)
        gtk_timeout_remove (color_triangle->timer_tag);
      
      color_triangle->timer_tag = gtk_timeout_add (TIMER_DELAY,
                                                   (GtkFunction) gtk_color_triangle_value_timeout,
                                                   (gpointer) color_triangle);
      break;
    case GTK_UPDATE_DISCONTINUOUS:
    default:
      break;
    }
  
  gtk_widget_queue_draw(widget);

  return TRUE;
}


/*
 * FIXME this drawing code is really slow
 *
 * - use LUT to avoid hsv_to_color translations per-pixel
 * - avoid multiple malloc() of row buffers
 * - delete duplicate code at the front of each function
 */

static void
copy_row(GtkColorTriangle* color_triangle, guchar* buf, gint x, gint y, gint w)
{
  gint xoffset, yoffset;
  guchar* target;

  /* GtkPreview did this so the old drawing code depends on it */
  if (x < 0) 
    return;

  if ((x + w) > color_triangle->wheel_radius*2)
    w = color_triangle->wheel_radius*2 - x;

  if (w <= 0)
    return;
  
  if (y < 0 || (y > (color_triangle->wheel_radius*2+1)))
    return;
  
  xoffset = (GTK_WIDGET(color_triangle)->allocation.width - (color_triangle->wheel_radius*2))/2;
  yoffset = (GTK_WIDGET(color_triangle)->allocation.height - (color_triangle->wheel_radius*2))/2;

  target = color_triangle->buf + (y+yoffset)*color_triangle->rgb_rowstride + (x+xoffset)*3;

  g_assert(target < (color_triangle->buf + (color_triangle->rgb_rowstride*color_triangle->rgb_height)));

  memcpy(target, buf, w * 3);
}

static void
draw_triangle_and_wheel(GtkColorTriangle* color_triangle)
{
  guchar* buf;
  gint y;
  gdouble r2, val, sat, hue;
  gint hx,hy, sx,sy, vx,vy;
  
  hue = (double) color_triangle->values[HUE] * M_PI / 180;
    
  hx = sin(hue) * color_triangle->triangle_radius;
  hy = cos(hue) * color_triangle->triangle_radius;

  sx = sin(hue - 2*M_PI/3) * color_triangle->triangle_radius;
  sy = cos(hue - 2*M_PI/3) * color_triangle->triangle_radius;

  vx = sin(hue + 2*M_PI/3) * color_triangle->triangle_radius;
  vy = cos(hue + 2*M_PI/3) * color_triangle->triangle_radius;

  hue = (double) color_triangle->values[HUE];

  buf = g_malloc(color_triangle->rgb_rowstride);
  
  y = color_triangle->wheel_radius;
  while (y > - color_triangle->wheel_radius)
    {
      gint dx, x, k;
      
      dx = sqrt((double) (abs( (color_triangle->wheel_radius *
                                color_triangle->wheel_radius) -
                               (y * y) )) );
      
      x = -dx;
      k = 0;
      
      while (x <= dx && k < color_triangle->rgb_rowstride)
        {
          buf[k] = buf[k+1] = buf[k+2] = BGCOLOR;
          
          r2 = (x*x)+(y*y);

          if ( r2 <= (color_triangle->wheel_radius * color_triangle->wheel_radius))
            {
              if (r2 > (color_triangle->triangle_radius * color_triangle->triangle_radius))
                {
                  color_hsv_to_rgb (atan2 (x,y) / M_PI * 180, 1, 1, &buf[k], &buf[k+1], &buf[k+2]);
                }
              else
                {
                  val = (double) ((x-sx)*(hy-vy)-(y-sy)*(hx-vx)) / (double) ((vx-sx)*(hy-vy)-(vy-sy)*(hx-vx));
                  if (val > 0 && val <= 1) /* eigentlich val>=0, aber dann Grafikfehler... */
                    {
                      if (val == 0)
                        sat = 0;
                      else
                        sat = ((double) (y-sy-val*(vy-sy)) / (val * (double) (hy-vy)));
                      
                      if (sat >= 0 && sat <= 1)
                        {
                          color_hsv_to_rgb (hue, sat, val, &buf[k], &buf[k+1], &buf[k+2]);
                        }
                    }
                }
            }
          
          k += 3;
          ++x;
        }

      copy_row(color_triangle, buf,
               (color_triangle->wheel_radius - dx),
               (color_triangle->wheel_radius - y - 1),
               2*dx+1);
      --y;
    }

  g_free(buf);
}


static void
draw_wheel_marker(GtkColorTriangle* color_triangle)
{
  gint x0, y0;
  gdouble r2, hue;
  guchar* buf;
  guchar col;
  gint hx,hy, sx,sy, vx,vy;
  gint x, y, k;
  
  hue = (double) color_triangle->values[HUE] * M_PI / 180;
    
  hx = sin(hue) * color_triangle->triangle_radius;
  hy = cos(hue) * color_triangle->triangle_radius;

  sx = sin(hue - 2*M_PI/3) * color_triangle->triangle_radius;
  sy = cos(hue - 2*M_PI/3) * color_triangle->triangle_radius;

  vx = sin(hue + 2*M_PI/3) * color_triangle->triangle_radius;
  vy = cos(hue + 2*M_PI/3) * color_triangle->triangle_radius;

  hue = (double) color_triangle->values[HUE];
  
  buf = g_malloc(color_triangle->rgb_rowstride);
  
  x0 = (gint) (sin(hue*M_PI/180) * ((double) (color_triangle->wheel_radius - color_triangle->triangle_radius + 1)/2 + color_triangle->triangle_radius) + 0.5);
  y0 = (gint) (cos(hue*M_PI/180) * ((double) (color_triangle->wheel_radius - color_triangle->triangle_radius + 1)/2 + color_triangle->triangle_radius) + 0.5);

  color_hsv_to_rgb (atan2 (x0,y0) / M_PI * 180, 1, 1, &buf[0], &buf[1], &buf[2]);

  col = INTENSITY(buf[0], buf[1], buf[2]) > 127 ? 0 : 255;
  
  for (y = y0 - 4 ; y <= y0 + 4 ; y++)
    {

      for (x = x0 - 4, k=0 ; x <= x0 + 4 ; x++)
        {
          r2 = (x-x0)*(x-x0)+(y-y0)*(y-y0);
        
          if (r2 <= MARKER_SIZE && r2 >= 6)
            buf[k]=buf[k+1]=buf[k+2]=col;
          else
            color_hsv_to_rgb (atan2 (x,y) / M_PI * 180, 1, 1, &buf[k], &buf[k+1], &buf[k+2]);
          
          k += 3;
        }
      
      copy_row (color_triangle, buf,
                color_triangle->wheel_radius + x0 - 4, color_triangle->wheel_radius - 1 - y,
                9);
    }

  g_free(buf);
}

static void
erase_triangle_marker(GtkColorTriangle* color_triangle)
{
  gint x0, y0;
  gdouble r2, hue;
  guchar* buf;
  gint hx,hy, sx,sy, vx,vy;
  gdouble s, v, val, sat;
  gint x, y, k;
  
  hue = (double) color_triangle->values[HUE] * M_PI / 180;
    
  hx = sin(hue) * color_triangle->triangle_radius;
  hy = cos(hue) * color_triangle->triangle_radius;

  sx = sin(hue - 2*M_PI/3) * color_triangle->triangle_radius;
  sy = cos(hue - 2*M_PI/3) * color_triangle->triangle_radius;

  vx = sin(hue + 2*M_PI/3) * color_triangle->triangle_radius;
  vy = cos(hue + 2*M_PI/3) * color_triangle->triangle_radius;

  hue = (double) color_triangle->values[HUE];
  
  buf = g_malloc(color_triangle->rgb_rowstride);
  
  s = color_triangle->oldsat;
  v = color_triangle->oldval;
  
  x0 = (gint) (sx + (vx - sx)*v + (hx - vx) * s * v);
  y0 = (gint) (sy + (vy - sy)*v + (hy - vy) * s * v);
  
  for (y = y0 - 4 ; y <= y0 + 4 ; y++)
    {
      for (x = x0 - 4, k=0 ; x <= x0 + 4 ; x++)
        {
          buf[k]=buf[k+1]=buf[k+2]=BGCOLOR;
          r2 = (x-x0)*(x-x0)+(y-y0)*(y-y0);
          
          if (x*x+y*y > color_triangle->triangle_radius * color_triangle->triangle_radius)
            { 
              color_hsv_to_rgb (atan2 (x,y) / M_PI * 180, 1, 1, &buf[k], &buf[k+1], &buf[k+2]);
            }
          else
            {
              val = (double) ((x-sx)*(hy-vy)-(y-sy)*(hx-vx)) / (double) ((vx-sx)*(hy-vy)-(vy-sy)*(hx-vx));
              if (val>0 && val<=1)
                {   /* eigentlich val>=0, aber dann Grafikfehler... */
                  sat = (val==0?0: ((double) (y-sy-val*(vy-sy)) / (val * (double) (hy-vy))));
                  if (sat >= 0 && sat <= 1) 
                    color_hsv_to_rgb (hue, sat, val, &buf[k], &buf[k+1], &buf[k+2]);
                } 
            }
          k += 3;
        }
      copy_row (color_triangle, buf,
                color_triangle->wheel_radius + x0-4, color_triangle->wheel_radius - 1 - y, 9);
    }
  
  color_triangle->oldsat = color_triangle->values[SATURATION] / 100.0;
  color_triangle->oldval = color_triangle->values[VALUE] / 100.0;

  g_free(buf);
}

static void
draw_triangle_marker(GtkColorTriangle* color_triangle)
{
  gint x0, y0;
  gdouble r2, hue;
  guchar* buf;
  guchar col;
  gint hx,hy, sx,sy, vx,vy;
  gint x, y, k;
  gdouble s, v, val, sat;
  
  hue = (double) color_triangle->values[HUE] * M_PI / 180;
    
  hx = sin(hue) * color_triangle->triangle_radius;
  hy = cos(hue) * color_triangle->triangle_radius;

  sx = sin(hue - 2*M_PI/3) * color_triangle->triangle_radius;
  sy = cos(hue - 2*M_PI/3) * color_triangle->triangle_radius;

  vx = sin(hue + 2*M_PI/3) * color_triangle->triangle_radius;
  vy = cos(hue + 2*M_PI/3) * color_triangle->triangle_radius;

  hue = (double) color_triangle->values[HUE];
  
  buf = g_malloc(color_triangle->rgb_rowstride);
  
  col = INTENSITY(color_triangle->values[RED], color_triangle->values[GREEN],
                  color_triangle->values[BLUE]) > 127 ? 0 : 255 ;
  
  s = color_triangle->values[SATURATION] / 100.0;
  v = color_triangle->values[VALUE] / 100.0;
  color_triangle->oldsat = s;
  color_triangle->oldval = v;
  x0 = (gint) (sx + (vx - sx)*v + (hx - vx) * s * v);
  y0 = (gint) (sy + (vy - sy)*v + (hy - vy) * s * v);

  for (y = y0 - 4 ; y <= y0 + 4 ; y++)
    {
      for (x = x0 - 4, k=0 ; x <= x0 + 4 ; x++)
        {
          buf[k]=buf[k+1]=buf[k+2]=BGCOLOR;
          r2 = (x-x0)*(x-x0)+(y-y0)*(y-y0);
          if (r2 <= MARKER_SIZE && r2 >= 6)
            buf[k]=buf[k+1]=buf[k+2]=col;
          else
            {
              if (x*x+y*y > color_triangle->triangle_radius * color_triangle->triangle_radius)
                { 
                  color_hsv_to_rgb (atan2 (x,y) / M_PI * 180, 1, 1, &buf[k], &buf[k+1], &buf[k+2]);
                }
              else
                {
                  val = (double) ((x-sx)*(hy-vy)-(y-sy)*(hx-vx)) / (double) ((vx-sx)*(hy-vy)-(vy-sy)*(hx-vx));
                  if (val>0 && val<=1)
                    {   /* eigentlich val>=0, aber dann Grafikfehler... */
                      sat = (val==0?0: ((double) (y-sy-val*(vy-sy)) / (val * (double) (hy-vy))));
                      if (sat >= 0 && sat <= 1) 
                        color_hsv_to_rgb (hue, sat, val, &buf[k], &buf[k+1], &buf[k+2]);
                    } 
                }
            } 
          k += 3;
        }
      copy_row (color_triangle, buf,
                color_triangle->wheel_radius + x0-4, color_triangle->wheel_radius - 1 - y, 9);
    }

  g_free(buf);
}

static gint
gtk_color_triangle_expose (GtkWidget      *widget, GdkEventExpose *event)
{
  GtkColorTriangle* color_triangle;
  
  color_triangle = GTK_COLOR_TRIANGLE(widget);

  if (!GTK_WIDGET_DRAWABLE (widget))
    return FALSE;

  g_return_val_if_fail(widget->window != NULL, FALSE);
  
  if (color_triangle->update_pending &&
      color_triangle->rgb_width > 0)
    {
      if (color_triangle->hue_has_changed)
        {
          draw_triangle_and_wheel(color_triangle);
          draw_wheel_marker(color_triangle);
          color_triangle->hue_has_changed = FALSE;          
        }
      else
        {
          erase_triangle_marker(color_triangle);
        }

      draw_triangle_marker(color_triangle);
      
      color_triangle->update_pending = FALSE;
    }
      
  gdk_draw_rgb_image(widget->window, widget->style->white_gc,
                     0, 0, color_triangle->rgb_width,
                     color_triangle->rgb_height,
                     GDK_RGB_DITHER_NORMAL,
                     color_triangle->buf,
                     color_triangle->rgb_rowstride);
  
  return FALSE;
}

/*
 * Public API
 */

GtkWidget*
gtk_color_triangle_new               (void)
{
  
  return GTK_WIDGET(gtk_type_new(gtk_color_triangle_get_type()));
}

void
gtk_color_triangle_set_update_policy (GtkColorTriangle     *color_triangle,
                                      GtkUpdateType         policy)
{
  g_return_if_fail (color_triangle != NULL);
  g_return_if_fail (GTK_IS_COLOR_TRIANGLE (color_triangle));
  
  color_triangle->policy = policy;
}

void
gtk_color_triangle_set_color         (GtkColorTriangle     *color_triangle,
                                      gdouble              *color)
{
  g_return_if_fail (color_triangle != NULL);
  g_return_if_fail (GTK_IS_COLOR_TRIANGLE (color_triangle));

  color_triangle->values[RED] = color[0] * 255.0;
  color_triangle->values[GREEN] = color[1] * 255.0;
  color_triangle->values[BLUE] = color[2] * 255.0;

  gtk_color_triangle_update_hsv_values(color_triangle);
}

void
gtk_color_triangle_get_color         (GtkColorTriangle     *color_triangle,
                                      gdouble              *color)
{
  g_return_if_fail (color_triangle != NULL);
  g_return_if_fail (GTK_IS_COLOR_TRIANGLE (color_triangle));

  color[0] = color_triangle->values[RED]/255.0;
  color[1] = color_triangle->values[GREEN]/255.0;
  color[2] = color_triangle->values[BLUE]/255.0;
}

