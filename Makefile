CFLAGS = `gnome-config --cflags gnomeui` -Wall -g -O2
LDFLAGS = `gnome-config --libs gnomeui`

colorsel: gtk-color-selection2.o gtkhsv.o
	$(CC) -o colorsel gtk-color-selection2.o gtkhsv.o $(LDFLAGS)

gtk-color-selection2.o: gtk-color-selection2.c

gtkhsv.o: gtkhsv.c
