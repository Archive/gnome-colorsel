#include <stdio.h>
#include <gtk/gtk.h>
#include "gtkhsv.h"

static GtkAdjustment *hadj, *sadj, *vadj;

static void
hsv_changed (GtkHSV *hsv, gpointer data)
{
	double h, s, v;

	gtk_hsv_get_color (hsv, &h, &s, &v);
	printf ("Changed %g %g %g, %sadjusting\n", h, s, v,
		gtk_hsv_is_adjusting (hsv) ? "" : "not");
}

static void
adj_changed (GtkAdjustment *adj, gpointer data)
{
	gtk_hsv_set_color (GTK_HSV (data), hadj->value, sadj->value, vadj->value);
}


static void
size_changed (GtkAdjustment *adj, gpointer data)
{
	int ring_width;

	gtk_hsv_get_metrics (GTK_HSV (data), NULL, &ring_width);
	gtk_hsv_set_metrics (GTK_HSV (data), adj->value, ring_width);
}

static void
ring_width_changed (GtkAdjustment *adj, gpointer data)
{
	int size;

	gtk_hsv_get_metrics (GTK_HSV (data), &size, NULL);
	gtk_hsv_set_metrics (GTK_HSV (data), size, adj->value);
}

int
main (int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *hsv;
	GtkAdjustment *adj;
	GtkWidget *w;

	gtk_init (&argc, &argv);
	gdk_rgb_init ();

	gtk_widget_push_visual (gdk_rgb_get_visual ());
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	hsv = gtk_hsv_new ();
	gtk_signal_connect (GTK_OBJECT (hsv), "changed",
			    GTK_SIGNAL_FUNC (hsv_changed),
			    NULL);
	
	gtk_widget_pop_visual ();
	gtk_widget_pop_colormap ();

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_border_width (GTK_CONTAINER (vbox), 10);
	gtk_container_add (GTK_CONTAINER (window), vbox);

	gtk_box_pack_start (GTK_BOX (vbox), hsv, TRUE, TRUE, 0);

	/* Hue */

	hadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 1.0, 0.01, 0.01, 0.01));
	w = gtk_spin_button_new (hadj, 0.0, 2);
	gtk_signal_connect (GTK_OBJECT (hadj), "value_changed",
			    GTK_SIGNAL_FUNC (adj_changed),
			    hsv);

	gtk_box_pack_start (GTK_BOX (vbox), w, FALSE, FALSE, 0);

	/* Saturation */

	sadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 1.0, 0.01, 0.01, 0.01));
	w = gtk_spin_button_new (sadj, 0.0, 2);
	gtk_signal_connect (GTK_OBJECT (sadj), "value_changed",
			    GTK_SIGNAL_FUNC (adj_changed),
			    hsv);

	gtk_box_pack_start (GTK_BOX (vbox), w, FALSE, FALSE, 0);

	/* Value */

	vadj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 1.0, 0.01, 0.01, 0.01));
	w = gtk_spin_button_new (vadj, 0.0, 2);
	gtk_signal_connect (GTK_OBJECT (vadj), "value_changed",
			    GTK_SIGNAL_FUNC (adj_changed),
			    hsv);

	gtk_box_pack_start (GTK_BOX (vbox), w, FALSE, FALSE, 0);

	/* Size */

	adj = GTK_ADJUSTMENT (gtk_adjustment_new (100, 0.0, 300.0, 1, 1, 1));
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			    GTK_SIGNAL_FUNC (size_changed),
			    hsv);

	w = gtk_spin_button_new (adj, 0, 0);
	gtk_box_pack_start (GTK_BOX (vbox), w, FALSE, FALSE, 0);

	/* Ring width */

	adj = GTK_ADJUSTMENT (gtk_adjustment_new (10, 0.0, 300.0, 1, 1, 1));
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			    GTK_SIGNAL_FUNC (ring_width_changed),
			    hsv);

	w = gtk_spin_button_new (adj, 0, 0);
	gtk_box_pack_start (GTK_BOX (vbox), w, FALSE, FALSE, 0);
	
	gtk_widget_show_all (window);
	gtk_main ();
	return 0;
}
